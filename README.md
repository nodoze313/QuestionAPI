This repository contains the public Question API.


# PHP API #




# Rest API #
## POST /login ##

Accepts form postdata

string username - username
string password - base64 encoded password

## POST /question/manage/create ##

Accepts form post data

Attributes
string question - bbcode question for users
array answer - json array string of answer strings
integer quorumSize - minimum entities in quorum
integer quorumConfidence - minimum percent agreement

Returns id of question.


## POST  /question/manage/edit ##

Accepts form post data

Attributes
integer qid - id of the question to edit
string question - bbcode question for users
array answer - json array string of answer strings
integer quorumSize - minimum entities in quorum
integer quorumConfidence - minimum percent agreement


## POST  /question/manage/delete ##

Accepts form post data

Attributes
array qids - question ids to be deleted

Does not return.

## GET /question/manage/delete/{qid} ##

Attributes
integer qid - question id to delete



